---
layout: default
title: Info for parents
permalink: /parents/
---

### Do you have a kid who is in this year's coding club?

Thanks for lending me your kids' attention for a few hours! Here are some quick notes:

* Your kids are all awesome, and I'm happy I volunteered.
* Each student will retain their repl.it accounts, and can continue to modify and improve their games.
* I am available for any help the students might need with this content after the club ends. Just shoot me [an email](mailto:curtis@autery.net).
* I have a group on remind.com, @gemscoders, for club announcements.

### Are you interested in having a kid attend a future club?

Each club will be different. The current (informal) plan is for Mr. Wiseman to order a set of [Circuit Playground](https://www.adafruit.com/product/3000) developer boards to give students for one club per year, where we'll run through Code.org's "physical computing" curriculum. Other clubs will have different themes - building text games, simple animations, and whatever anyone pitches me that sounds achievable and fun.

I'm looking forward to doing more volunteering at GEMS, and I hope to see your kid in a future class!


Curtis

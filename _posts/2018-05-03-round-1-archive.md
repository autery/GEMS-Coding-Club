---
layout: post
title:  "Projects and videos from Coding Club round 1"
date:   2018-05-03
categories: main
---

## Videos

### Have two Circuit Playgrounds communicate

<iframe width="560" height="315" src="https://www.youtube.com/embed/SKFb9F4Xxb8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<!--more-->

### Connecting to external LEDs

<iframe width="560" height="315" src="https://www.youtube.com/embed/27h0kdj2qNA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### Animated pseudo-lights

<iframe width="560" height="315" src="https://www.youtube.com/embed/0YhbWhd_dM0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

This video used some variables I cut and pasted from Wolfram programming lab. If you want to use them in your project, here they are:

{% highlight javascript %}
var xPoints = [193, 248, 270, 248, 193, 126, 71, 50, 71, 126];
var yPoints = [120, 160, 225, 289, 329, 329, 289, 225, 160, 120];
{% endhighlight %}

### Timed loops

<iframe width="560" height="315" src="https://www.youtube.com/embed/Hs1EB8dQb8w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Code.org Code Studio projects

### [Fly](https://studio.code.org/projects/makerlab/VXZW6EKwx7cckX48MthjJ0JxE9mT7NXRan-olJs_u_s/view)

* Move a circle around the screen based on how level you hold the Circuit Playground.

### [Dodge!](https://studio.code.org/projects/makerlab/LYszAfI_oNtGsC-xjN01W9-GkqSAeEDm9gsvri6Jxg0/view)

* This adds collision detection to the Fly project, for a makeshift video game.

### [Digital writes](https://studio.code.org/projects/applab/Sr_iY0czxQEvqDpB2fSA4G_SAe5T9c85nO5Dix8NM2I/view)

* This is a simple demo of setting a pin to digital write mode, and toggling it on and off with a button.

### [Accelerometer](https://studio.code.org/projects/makerlab/ICQgNQUPjPw-YswIbqymC9blzGnLyJgpE1Uncu46ilc/view)

* Show the Circuit Playground's current pitch, inclination, and roll values.

### [Lowest Light](https://studio.code.org/projects/makerlab/aAvZHB3b27MYZKpjyn7PE2Pwu72jr-QHe5C82_CWiLI/view)

* Use the accelerometer to displays the inclination value, and light up the ColorLED that is lowest.

### [Loops](https://studio.code.org/projects/applab/SuWlYTPzpdNaHP9U6L0O2WvmMlHi2ei1xHvg2-VMjdU/view)

* This shows a timed loop iterating through each ColorLED, and draws on a canvas a representation of which light on the board should be lit.

### [Cyclons talking](https://studio.code.org/projects/applab/xLB-XB0HBAsy6gK_0LPzYLyw2veZ2VLtTYgWubVMWco/view)

* Working code from the "Have two Circuit Playgrounds communicate" video.

### [Pong](https://studio.code.org/projects/applab/poRWCA5gjzq4kt0g3iiyKF6yNtjibds4DSu81F-Y2Eg/view)

* Simple Pong-like game where one player is controlled with Circuit Playground buttons, the with arrow keys

## Arduino projects

### [Firmata](https://create.arduino.cc/editor/ceautery/a4064839-94ed-4463-8f67-b70016a59a37/preview)

* The firmware that allows Code Studio to talk to the Circuit Playground. After playing with the Arduino projects below, you can load the Firmata firmware back onto your Circuit Playground to use Code Studio again.

### [Star Trek comm badge](https://create.arduino.cc/editor/ceautery/1e18227d-ec59-4db6-84f2-c818e272404e/preview)

* Tap the Circuit Playground to hear the Trek comm badge sound

### [Simon client](https://create.arduino.cc/editor/ceautery/fe0e5a7a-aae2-48a6-9cd6-928f792c7e0a/preview)

* Play a game of Simon with multiple Circuit Playgrounds each acting as a single button. Requires loading the Simon server project onto an Adafruit Gemma M0... which I haven't written yet. Coming soon, I hope.

### [Serial Send](https://create.arduino.cc/editor/ceautery/b2ebed84-3991-4a36-b3ac-157b52623243/preview)
### [Serial Receive](https://create.arduino.cc/editor/ceautery/41d36654-8921-4854-8732-970c49639eea/preview)

* A simple demo of using Serial1 to transmit a word ("button") over alligator-clip cable from one Circuit Playground to another. Use the serial port monitor on the receiving board to see what it receives. Press the left button of the sending board to send the message.

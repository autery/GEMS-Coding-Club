---
layout: post
title:  "Conway's Game of Life"
date:   2018-02-12
categories: main
---

### Conway's Game of Life

![Game of life](/assets/02-game-of-life/life.png)
<!--more-->

In 1970, Cambridge mathematician John Conway invinted a solitaire game he simply called [Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life), an early example of [Cellular Atomata](https://en.wikipedia.org/wiki/Cellular_automaton). In the game's world, you are on an infinite grid, and the cells of each grid are either alive or dead. On each round, some cells die, and others are born, depending on how many of their 8 neighbors are alive. The rules are simple:

1. A live cell remains alive only if it has 2 or 3 live neighbors.
  * Less than two means it dies of loneliness, more then 3 means it dies of overcrowding.
2. A cell that isn't alive is "born" if it has exactly 3 live neighbors.

With those simple rules, a surprisingly complex ecosystem arises.

Here is a simple example of a "blinker", a pattern that cycles between two states between rounds. On the first round we have:

<pre>
               --- --- --- --- ---
              |   |   |   |   |   |
               --- --- --- --- ---
              |   |   | X |   |   |
               --- --- --- --- ---
              |   |   | X |   |   |
               --- --- --- --- ---
              |   |   | X |   |   |
               --- --- --- --- ---
              |   |   |   |   |   |
               --- --- --- --- ---
</pre>

Three cells turned on in a vertical line. To the left and right of the middle cell, we have empty cells that have exactly three neighbors. On the next round these will be born (B). On the top and bottom, we have living cells which only have 1 neighbor: the middle cell. These cells will die next round (D).

<pre>
 --- --- --- --- ---        --- --- --- --- --- 
|   |   |   |   |   |      |   |   |  |   |   |
 --- --- --- --- ---        --- --- --- --- ---
|   |   | X |   |   |      |   |   | D |   |   |
 --- --- --- --- ---        --- --- --- --- --- 
|   | B | X | B |   |      |   |   | X |   |   |
 --- --- --- --- ---        --- --- --- --- --- 
|   |   | X |   |   |      |   |   | D |   |   |
 --- --- --- --- ---        --- --- --- --- --- 
|   |   |   |   |   |      |   |   |   |   |   |
 --- --- --- --- ---        --- --- --- --- --- 
</pre>


On round 2, we keep the middle cell (it had 2 living neighbors, so it stays alive), delete the dead cells, and add the born cells, giving:

<pre>
             --- --- --- --- ---
            |   |   |   |   |   |
             --- --- --- --- ---
            |   |   |   |   |   |
             --- --- --- --- ---
            |   | X | X | X |   |
             --- --- --- --- ---
            |   |   |   |   |   |
             --- --- --- --- ---
            |   |   |   |   |   |
             --- --- --- --- ---
</pre>

On round three, the same thing happens again, swapping horizontal and vertical. Below is a simulation with a handful of interesting starting points that you can play with to see the game board unfold.

<script src="{{ "/assets/02-game-of-life/life.js" | relative_url }}"></script>
<canvas width="600" height="600" style="border: 1px solid;"></canvas>

<pre>
  Directions:

  S to start/stop simulation
  Arrow keys to pan
  - and = to zoom 
  [ and ] to adjust speed
  1 through 7 to load preset boards
</pre>

An interesting article worth reading on the Game of Life is from the [Mathematical Games]({{ "/assets/pdfs/Gardner_GameofLife10-1970.pdf" | relative_url }}) section of Scientific American from October 1970, written by one of my favorite layman's math writers, Martin Gardner.

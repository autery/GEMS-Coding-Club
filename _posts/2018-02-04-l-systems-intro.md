---
layout: post
title:  "L-Systems"
date:   2018-02-04
categories: main
---

### Get your fractal on!

![Dragon curve](/assets/01-l-systems/dragon.png)

<!--more-->
In 1968, biologist Aristid Lindenmayer wrote a pair of papers describing the growth of algae, in particular how larger neighborhoods of cells grew in similar ways to individual cells. He developed a notation for different types of growth behavior, that ended up being a great basis for drawing fractals.

The basic idea is that, given a starting sequence of characters, and a small set of search-and-replace rules, the "sentence" would grow over multiple rounds. Certain letters could then correspond to moves and turns, to make interesting shapes that become more complex each round.

For example, let's say `F` and `H` both mean to move forward, `+` and `-` mean to turn right and left 90 degrees, respectively, and my sentence starts with just `H`. For each new round, each `H` is replaced with `F+H`, and each `F` is replaced with `F-H`. Here is how the sentence changes each round:

<pre>
1: F
2: F-H
3: F-H-F+H
4: F-H-F+H-F-H+F+H
</pre>

And here are the sentences if we draw them instead:

![Dragon curve first 4 steps](/assets/01-l-systems/dragon-steps.png)


If you continue this sequence for a few more steps, the curve gets more complicated, and eventually turns into the image at the top of this post. If you repeatedly fold a piece of paper in half along the same axis, it can be unfolded to have the same pattern as the curve:

![Dragon curve on paper](/assets/01-l-systems/dragon-paper.jpg)

Below are some buttons that show the rules and first few rounds of some popular fractals. Click the "show canvas" checkbox before clicking start if you want to see them drawn one round at a time. The drawing should go fullscreen, and clicking advances one round at a time. Just hit escape to exit.

Enjoy!<br>
Curtis

<style>
  button {
    margin: 10px;
    padding: 10px;
    font-size: 14px;
  }
  .output {
    font: 20px Courier new;
    white-space: pre;
  }
</style>
<canvas id="cnv"></canvas>
<div id="inputs">
</div>
<div id="description" class="output">
</div>
<button onclick="setupCanvas()">Start</button>
<input id="showCanvas" type="checkbox" /> Show Canvas
<div id="log" class="output">
</div>
<script src="{{ "/assets/01-l-systems/script.js" | relative_url }}"></script>

In Vihart's amazing series "Doodling in Math Class", she covers many of the L-System fractals above, in a way that's much more entertaining than a day at Coding Club:

<iframe width="560" height="315" src="https://www.youtube.com/embed/EdyociU35u8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

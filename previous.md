---
layout: default
title: Previous rounds
permalink: /previous/
---

### Round 1
#### Jan 29 - March 7, 2018

The inaugural Coding Club focused on the "physical computing" unit of code.org's [Computer Science Discoveries](https://studio.code.org/courses/csd?section_id=1389963) curriculum. Students used [Adafruit Circuit Playground Classic](https://www.adafruit.com/product/3000) developer boards, and programmed them using code.org's [Code Studio](https://studio.code.org/), which uses a custom version of Google's [Blockly](https://developers.google.com/blockly/) language.

Some sample projects and coding videos using Code Studio with Circuit Playground boards are available [here]({{ site.baseurl }}{% post_url 2018-05-03-round-1-archive %})

Special thanks to Mrs. Baginski, for teaching with me, Mr. Wiseman for a mountain of technical help, Mrs. Bachman for lots of feedback and support, and all of them (and all 15 students) for helping me feel welcome in the school.

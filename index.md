---
layout: default
---

### Welcome!

This site has content for the Coding Clubs at [Graham Expeditionary Middle School](http://www.thegrahamfamilyofschools.org/gems_home.aspx).

I'm Curtis, a software engineer for [Mutually Human](https://www.mutuallyhuman.com/), campus instructor for [Girls Who Code](https://girlswhocode.com/), and occasional school volunteer. If you have a student at GEMS interested in, or curious about tech, I'd love for them to join one of my clubs, or come visit Mutualy Human's [Hack Night](https://www.meetup.com/Mutually-Human-Hack-Night), on the first Thursday of each month.

### Coding club round 2: Build a text adventure!

Welcome to the second conding club of the 2017/2018 school year. We'll be creating an old-school text adventure game in the spirit of Zork, using the Python programming language.

I will help you build a game engine, but each game will be unique, based on content you'll create. This site will get some major updates before our second meeting, but for now, let's get you signed up with a repl.it account that let's you access my virtual classroom:

[Invitation link](https://repl.it/classroom/invite/UYXk8dW)

Once you're signed up, take a look at my [Sample project](https://repl.it/@curtisautery/Coding-club-demo) to see what type of code we'll be writing.

Here's a (cheesy) example of the type of game we'll be building:

<iframe width="560" height="315" src="https://www.youtube.com/embed/bOm9wt1Hnc0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Enjoy!

[Curtis Autery](mailto:curtis@autery.net)

function toggleBurger(burger) {
  var f = burger.classList.contains('is-active') ? 'remove' : 'add';
  burger.classList[f]('is-active');
  document.getElementById('menu').classList[f]('is-active');
}
